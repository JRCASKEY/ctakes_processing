ARR=($(<cTAKES_radiology_smFileSize.dirList.txt))
TSTRING=$(date +"%m%d%y_%H%M%S")
let CT=0
let X=0
ROOTDIR=$PWD
for i in "${ARR[@]}" ; do
  # debug and test
  if ((CT==14)) ; then
    echo '14 jobs started, pausing to wait for completion'
    echo 'job ids are:'
    # ps aux | grep java | grep remaining_cTAKES_files_smFileSize | cut -d\  -f2
    wait %1 %2 %3 %4 %5 %6 %7 %8 %9 %10 %11 %12 %13 %14
    echo 'batch of jobs finished'
    ARRV=($(ls | grep 'ctakes_4.0.0.1_instance' | sort -V))
    for x in "${ARRV[@]}" ; do
      rm -rf ${x}/inputDir
      sleep 5
      mkdir ${x}/inputDir
      # gzip XMI output files
      bash gzipFiles.sh ${x}/outputDir
    done
    echo 'setup cTAKES directories for new jobs'
    let X=0
    let CT=1
    cp -r ${i} ctakes_4.0.0.1_instance_${X}/inputDir/
    sleep 2
    cd ctakes_4.0.0.1_instance_${X}
    ./bin/runPiperFile.mod.sh -p resources/org/apache/ctakes/clinical/pipeline/DefaultFastPipeline.modOut2.piper -l resources/org/apache/ctakes/dictionary/lookup/fast/custom_snomed.xml -i inputDir/ --xmiOut outputDir 1>> ctakes_log.${TSTRING}.txt 2>> ctakes_errlog.${TSTRING}.txt &
    cd $ROOTDIR
    let X=$X+1
  else
    cp -r ${i} ctakes_4.0.0.1_instance_${X}/inputDir/
    cd ctakes_4.0.0.1_instance_${X}
    ./bin/runPiperFile.mod.sh -p resources/org/apache/ctakes/clinical/pipeline/DefaultFastPipeline.modOut2.piper -l resources/org/apache/ctakes/dictionary/lookup/fast/custom_snomed.xml -i inputDir/ --xmiOut outputDir 1>> ctakes_log.${TSTRING}.txt 2>> ctakes_errlog.${TSTRING}.txt &
    cd $ROOTDIR
    let X=$X+1
    let CT=$CT+1
    echo "started cTAKES for $i"
    sleep 30
  fi
done
echo 'monitor remaining jobs for completion'

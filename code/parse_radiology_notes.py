import os
# use for debugging if error appears
# os.environ['OPENBLAS_NUM_THREADS'] = '1'
import pandas as pd
from tqdm import tqdm

print('importing data of 28 iterations')
NOTES_FILE='Churpek_IMPRESSION_NARRATIVE_2020_11_20.csv'
frame_data = []
for df_chunk in tqdm(pd.read_csv(NOTES_FILE, chunksize=100000, error_bad_lines=False, engine='python')):
  frame_data.append(df_chunk)
df = pd.concat(frame_data)
df_len = len(df.index)
print(f'dataframe has {df_len} values')
pat_id_list = df['PSEUDO_PAT_ENC_CSN_ID'].to_list()
pat_id_list = list(set(pat_id_list))
pat_id_list.sort()

print('found this many unique identifiers:')
print(len(pat_id_list))

print('writing to individual output CSV files')
counter = 0
for i in tqdm(pat_id_list):
  df_tmp = df.loc[df['PSEUDO_PAT_ENC_CSN_ID'].isin([i])]
  # print(df_tmp)
  counter += len(df_tmp.index)
  for x in range(len(df_tmp.index)):
    idx = x+1
    fName = 'output_parse/' + str(i) + '.' + str(idx) + '.csv'
    df_tmp.iloc[[x]].to_csv(fName, index=False)
print(f'found total of {counter} values')

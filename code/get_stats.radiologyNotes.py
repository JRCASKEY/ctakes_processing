import pandas as pd
from tqdm import tqdm

inputFile = 'Churpek_IMPRESSION_NARRATIVE_2020_11_20.csv'
dfList = []
for df_chunk in tqdm(pd.read_csv(inputFile, chunksize=100000, error_bad_lines=False, engine='python')):
  dfList.append(df_chunk)
df = pd.concat(dfList)

# get list of unique procedure names
note_names = df['PROC_NAME'].tolist()
note_names = list(set(note_names))

# count chars, must include NARRATIVE_TEXT and IMPRESSION_TEXT
df['NARRATIVE_TEXT'] = df['NARRATIVE_TEXT'].fillna('')
df['IMPRESSION_TEXT'] = df['IMPRESSION_TEXT'].fillna('')


df['CharCt_Narrative'] = df['NARRATIVE_TEXT'].str.len()
df['CharCt_Impression'] = df['IMPRESSION_TEXT'].str.len()
df['CharCtAll'] = df.apply(lambda row: row.CharCt_Narrative + row.CharCt_Impression, axis=1)
# need to drop last row on full dataset
df.drop(df.tail(1).index, inplace=True)
print(df)

# skip this step: verified that ORDER_TIME is never NaN
'''
print('checking for NaN in RESULT_TIME')
tmp = df.loc[df.RESULT_TIME.isnull()]
print(tmp[['PSEUDO_PAT_ENC_CSN_ID', 'RESULT_TIME']])

print('checking for NaN in ORDER_TIME')
tmp = df.loc[df.ORDER_TIME.isnull()]
print(tmp[['PSEUDO_PAT_ENC_CSN_ID', 'RESULT_TIME', 'ORDER_TIME']])
'''

timeList = df['ORDER_TIME'].tolist()
timeList = [str(x) for x in timeList]
x_list = [x.split() for x in timeList]
date_ts = [x[0] for x in x_list]
date_list = [x.split('-') for x in date_ts]
year_list = [x[0] for x in date_list]
df['YEAR'] = year_list
year_list_uniq = list(set(yearList))
year_list_uniq.sort()
print('list of years is')
[print(x) for x in year_list_uniq]

print('list of Procedure Names is')
[print(x) for x in note_names]

def percentile(n):
  # credit https://stackoverflow.com/a/17578653
  def percentile_(x):
    return np.percentile(x, n)
  percentile_.__name__ = 'percentile_%s' %n
  return percentile_

# df_grouped = df_all.groupby(['NOTE_TYPE'])['CumulativeCharCt'].agg([np.median, np.min, np.max, np.mean, percentile(25), percentile(75)]).reset_index()

df_yearStats = df.groupby(['YEAR'])['CharCtAll'].agg([np.median, np.min, np.max, np.mean, percentile(25), percentile(75)]).reset_index()
df_notetypeStats = df.groupby(['PROC_NAME'])['CharCtAll'].agg([np.median, np.min, np.max, np.mean, percentile(25), percentile(75)]).reset_index()

df_yearStats.to_csv('Churpek_IMPRESSION_NARRATIVE_2020_11_20.statsByYear.csv', index=False)
df_notetypeStats.to_csv('Churpek_IMPRESSION_NARRATIVE_2020_11_20.statsByProcName.csv', index=False)
print('job done')

import pandas as pd

# import each list of files, 
# then check for duplicates (there should not be any!)

def importList(f):
  l = []
  with open(f) as fOpen:
    for i in fOpen:
      i = i.rstrip('\r\n')
      l.append(i)
  return l

l0 = importList('absolute_noteID_list.03242022.txt')
l1 = importList('notFound_need_running.02142022.IDonly.txt')
l2 = importList('duplicated_need_rerunning.02142022.IDonly.txt')
l3 = importList('completed_readyForFlatfile.02142022.IDonly.txt')
l = l1 + l2 + l3
len_b4Check = len(l)
l = list(set(l))
len_afterCheck = len(l)
detectedCt = len_b4Check - len_afterCheck
if len_b4Check != len_afterCheck:
  print('removed ' + str(detectedCt) + ' duplicate entries from processed files')
df_processed = pd.DataFrame({'FileID' : l})
df_processed['FileID'] = df_processed['FileID'].astype(dtype={df_processed.columns[0] : str})
df_processed['X'] = 1
df_ref = pd.DataFrame({'FileID' : l0})
df_ref['FileID'] = df_ref['FileID'].astype(dtype={df_ref.columns[0] : str})
df_ref['X'] = 1
df_merged = pd.merge(df_ref, df_processed, how="outer", on="FileID")
df_missing = df_merged.loc[df_merged['X_x'] != df_merged['X_y'],]
df_found = df_merged.loc[df_merged['X_x'] == df_merged['X_y'],]
print('total entries:')
print(len(df_merged.index))
print('entries accounted for:')
print(len(df_found.index))
print('entries not accounted for:')
print(len(df_missing.index))
print('outputting list of missing IDs and confirmed processed IDs.')
df_missing = df_missing[['FileID']]
df_missing.to_csv('confirmed_missing.03242022.txt', index=False)
df_found.to_csv('confirmed_processed.03242022.txt', index=False)
